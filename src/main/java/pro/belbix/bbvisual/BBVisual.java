package pro.belbix.bbvisual;

import com.jme3.app.SimpleApplication;
import com.jme3.system.AppSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by v.belykh on 12.07.2018
 */
public class BBVisual extends SimpleApplication {
    private static final Logger log = LogManager.getRootLogger();


    public static void main(String[] args) {

        BBVisual app = new BBVisual();
        app.setShowSettings(false);
        app.setPauseOnLostFocus(false);

        AppSettings s = new AppSettings(true);
        s.setHeight(Config.height);
        s.setWidth(Config.width);

        app.setSettings(s);

        app.start();
    }

    @Override
    public void simpleInitApp() {

        MainScene neuronUpdater = new MainScene(assetManager);
        viewPort.attachScene(neuronUpdater.getRootNode());
        stateManager.attach(neuronUpdater);

        flyCam.setMoveSpeed(Config.moveSpeed);
    }

    @Override
    public void update() {
        super.update();

        float tpf = timer.getTimePerFrame();

        stateManager.update(tpf);
        stateManager.render(renderManager);

        renderManager.render(tpf, context.isRenderable());
    }

    private float rSpeed = 0;

    @Override
    public void loseFocus() {
        super.loseFocus();
        rSpeed = flyCam.getRotationSpeed();
        flyCam.setRotationSpeed(0);
    }

    @Override
    public void gainFocus() {
        super.gainFocus();
        if (rSpeed != 0) {
            flyCam.setRotationSpeed(rSpeed);
        }
    }
}
