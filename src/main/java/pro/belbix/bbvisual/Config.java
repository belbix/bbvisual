package pro.belbix.bbvisual;

/**
 * Created by v.belykh on 25.04.2018
 */
public class Config {

    //************************KAFKA ********************************



    //************************GRAPHIC********************************
    public static int width = 1280;
    public static int height = 780;
    public static int moveSpeed = 100;

    //************************NEURON BUILDER********************************

    public static float widthNeuron = 5;
    public static float widthDendrite = widthNeuron * 0.8f;
    public static float widthAxon = widthNeuron * 0.8f;
    public static float heightLayer = 10;
    public static float heightDendrite = heightLayer / 3;
    public static float heightAxon = heightLayer / 6;
    public static float heightReceiver = (heightLayer - heightDendrite) / 5;
    public static boolean zLines = true;

}
