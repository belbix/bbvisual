package pro.belbix.bbvisual;

import com.jme3.app.state.AbstractAppState;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.models.Key;
import pro.belbix.bbrain.models.State;
import pro.belbix.bbvisual.kafka.ConsumerCreator;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by v.belykh on 25.07.2018
 */
public class MainScene extends AbstractAppState {
    private static final Logger log = LogManager.getRootLogger();
    private Consumer<Long, State> consumer = ConsumerCreator.createConsumer();
    private AssetManager assetManager;
    private Node rootNode = new Node("NeuronUpdaterKafkaRootNode");
    private ConcurrentHashMap<Key, Node> nodes = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Spatial> globalSpatial = new ConcurrentHashMap<>();
    private int count = 0;
    private int countSkip = 0;
    private ExecutorService executorService = null;
    private List<Callable<Integer>> tasks = new ArrayList<>();
    private int threads = 10;


    public MainScene(AssetManager assetManager) {
        this.assetManager = assetManager;

        executorService = new ThreadPoolExecutor(threads, threads, 1000L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());

        for (int i = 0; i < threads; i++) {
            tasks.add(new Receiver(rootNode, nodes, globalSpatial, assetManager));
        }
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        LocalDateTime start = LocalDateTime.now();

        List<Future<Integer>> results = null;
        try {
            results = executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            log.error("", e);
        }
        int count = 0;
        int countThreads = 0;
        if (results != null) {
            for (Future<Integer> future : results) {
                try {
                    count += future.get();
                    countThreads++;
                } catch (InterruptedException | ExecutionException e) {
                    log.error("", e);
                }
            }
        }
        if(count > 0)
            log.info(countThreads + " threads count:" + count + " by time "
                + Duration.between(start, LocalDateTime.now()));


//        Callable<Boolean> receiver = () -> {
//            LocalDateTime start = LocalDateTime.now();
//
//            ConsumerRecords<Long, State> consumerRecords = consumer.poll(Duration.ofMillis(1));
//
//            count = 0;
//            countSkip = 0;
//            consumerRecords.forEach(record -> {
//                LocalDateTime rTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(record.timestamp()), ZoneOffset.systemDefault());
//                if (Duration.between(rTime, LocalDateTime.now()).getSeconds() > 5) {
//                    countSkip++;
//                    return;
//                }
//                State state = record.value();
//                count++;
//                Node node = nodes.get(state.getKey());
//                if (node == null) {
//                    node = new Node();
//                    rootNode.attachChild(node);
//                    nodes.put(state.getKey(), node);
//                }
//                NodeUpdater nodeUpdater = new NodeUpdater(node, globalSpatial, assetManager);
//                nodeUpdater.updateNodeFromState(state);
//            });
//            consumer.commitAsync();
//            if (count + countSkip > 0) {
//                log.debug("update " + count + " (skip " + countSkip + ") neurons of(" + nodes.size() + ") by time : "
//                        + Duration.between(start, LocalDateTime.now()));
//            }
//            return true;
//        };


        rootNode.updateLogicalState(tpf);
        rootNode.updateGeometricState();
    }

    public Node getRootNode() {
        return rootNode;
    }

    @Override
    public void cleanup() {
        super.cleanup();
        close();
        executorService.shutdown();
    }

    private void close() {
        consumer.close();
    }
}