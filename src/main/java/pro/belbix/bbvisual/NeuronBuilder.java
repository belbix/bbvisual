package pro.belbix.bbvisual;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Sphere;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.db.ConnectionPool;
import pro.belbix.bbrain.db.DBLayer;
import pro.belbix.bbrain.db.StateData;
import pro.belbix.bbrain.models.*;
import pro.belbix.bbrain.neuron.Layer;

import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

/**
 * Created by v.belykh on 13.07.2018
 */
public class NeuronBuilder implements Runnable {
    private static final Logger log = LogManager.getRootLogger();
    private AssetManager assetManager = null;
    private DBLayer dbLayer = null;
    private StateData stateData = null;

    private boolean run = true;
    private int updateDelay = 1;
    private float widthNeuron = 5;
    private float widthDendrite = widthNeuron * 0.8f;
    private float widthAxon = widthNeuron * 0.8f;
    private float heightLayer = 10;
    private float heightDendrite = heightLayer / 3;
    private float heightAxon = heightLayer / 6;
    private float heightReceiver = (heightLayer - heightDendrite) / 5;
    private boolean zLines = false;
    private long maxLayerCount = 0;

    private HashMap<String, State> states = null;
    private HashMap<String, Spatial> spatials = new HashMap<>();
    private HashMap<String, Node> nodes = new HashMap<>();
    private boolean readed = true;
    private HashMap<Key, Vector3f> receiversCoordinate = new HashMap<>();
    private HashMap<Long, Long> layersCount = new HashMap<>();
    private HashMap<String, Spatial> manipulators = new HashMap<>();

    public NeuronBuilder(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    @Override
    public void run() {
        LocalDateTime start;
        try {
            initDB();
        } catch (SQLException e) {
            log.error("", e);
        }
        try {
            while (run) {
                start = LocalDateTime.now();
                states = loadStates();
                log.debug("loadStates by time : " + Duration.between(start, LocalDateTime.now()));
//                log.info("loaded states");
                while (!readed && run) {
//                    log.info("wait updating");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        log.error("", e);
                    }
                }
                readed = false;
                try {
                    int count = 0;
                    while (count < updateDelay && run) {
                        Thread.sleep(1);
                        count++;
                    }
                } catch (InterruptedException e) {
                    log.error("", e);
                }
            }
        } finally {
            closeDB();
        }
    }

    public boolean readStates() {
        if (!readed) {
            if (states != null) {
                for (State state : states.values()) {
                    if (state == null || state.getNeuronModel() == null) continue;
                    long lId = state.getNeuronModel().getLayer_id();
                    long nId = state.getNeuronModel().getId();
                    Long maxNId = layersCount.get(lId);
                    if (maxNId == null) {
                        layersCount.put(lId, nId);
                    } else {
                        if (maxNId > maxLayerCount) maxLayerCount = maxNId;
                        if (nId > maxNId) layersCount.put(lId, nId);
                    }
                }

                for (String name : nodes.keySet()) {
                    if (!states.containsKey(name)) {
                        nodes.remove(name);
                        log.debug("remove node " + name);
                    }
                }

                manipulators.clear();
                for (State state : states.values()) {

                    if (state.getTransmitterModels() == null
                            || state.getNeuronModel() == null
                            || state.getSenderModels() == null
                            || state.getAxonModels() == null
                            || state.getDendriteModels() == null
                            || state.getReceiverModels() == null
                            || state.getReceptorModels() == null
                            || state.getLayerModel() == null) {
                        log.warn("Not valid state! " + state);
                        continue;
                    }

                    createGeometryFromState(state);
                }
            }
            readed = true;
            return true;
        }
        return false;
    }

    public void initDB() throws SQLException {
        dbLayer = new DBLayer();
        dbLayer.openConnection();
        stateData = new StateData(dbLayer);
        stateData.initPS();
    }

    public void closeDB() {
        stateData.close();
        dbLayer.close();
        ConnectionPool.getInstance().closePool();
    }

    public HashMap<String, State> loadStates() {
        if (stateData == null) return null;
        try {
            return stateData.getStates();
        } catch (SQLException e) {
            log.error("", e);
        }
        return null;
    }

    private void putToNode(Spatial spatial, String name) {
        Node node = nodes.get(name);
        if (node == null) {
            node = new Node();
            node.attachChild(spatial);
            nodes.put(name, node);
        } else {
//            if (node.getChild(spatial.getName()) != null) node.detachChildNamed(spatial.getName());
            node.attachChild(spatial);
        }
    }

    public void createGeometryFromState(State state) {
//        log.trace("start createGeometryFromState " + state);
        long lCount = layersCount.get(state.getNeuronModel().getLayer_id());
        long nId = state.getNeuronModel().getId();
        float xStart = ((maxLayerCount - lCount) / 2) * widthNeuron;
        if (zLines) xStart = FastMath.sqrt(((maxLayerCount - lCount) / 2) * widthNeuron);

        LayerModel layerModel = state.getLayerModel();
        if (layerModel == null) {
            log.warn("null layer " + state);
            return;
        }
        float line = FastMath.floor(FastMath.sqrt(layerModel.getNcount()));

        float nnZ = FastMath.floor(nId / line);
        long nnId = nId;
        if (zLines) nnId = nId - (long) (nnZ * line);

        if (Layer.LayerType.MANIPULATOR.ordinal() == state.getLayerModel().getType()) xStart = 0;


        float x = xStart + (nnId * widthNeuron);
        float y = (state.getNeuronModel().getLayer_id() * heightLayer);
        float z = 0;
        if (zLines) z = nnZ * widthNeuron;

        if (Layer.LayerType.MANIPULATOR.ordinal() == state.getLayerModel().getType()
                && manipulators.get(state.getManipulatorModel().getName()) == null) {
            createManipulator(state, x, y, z);
        }

        //--------------------NEURON BODY------------------------------
        createNeuronBody(state, x, y, z);

        //--------------------DENDRITE------------------------------
        createDendrite(state, x, y, z);

        //--------------------AXON------------------------------
        createAxon(state, x, y, z);


    }

    private void createManipulator(State state, float x, float y, float z) {
        ManipulatorModel manipulatorModel = state.getManipulatorModel();
        if (manipulatorModel == null) return;
        y += 4f;

        Geometry geo = (Geometry) spatials.get(manipulatorModel.getName());
        if (geo == null) {
            Sphere sphereMesh = new Sphere(8, 8, 2f);
            geo = new Geometry(manipulatorModel.getName(), sphereMesh);
            geo.setLocalTranslation(x, y, z);
            spatials.put(geo.getName(), geo);
            putToNode(geo, state.getNeuronModel().getName());
        }

        //-----TEXT-----
        BitmapText text = (BitmapText) spatials.get("text" + manipulatorModel.getName());
        if (text == null) {
            BitmapFont font = assetManager.loadFont("Interface/Fonts/Default.fnt");
            text = new BitmapText(font);
            text.setSize(0.5f);

            text.setColor(ColorRGBA.Gray);
            text.setLocalTranslation(x + 2, y + 2, z + 1);
            spatials.put("text" + manipulatorModel.getName(), text);
            putToNode(text, state.getNeuronModel().getName());
        }
        text.setText(geo.getName() + " " + manipulatorModel.getPower() + "/" + manipulatorModel.getResist() + " " + manipulatorModel.getDecision());

        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        if (manipulatorModel.getDecision()) {
            material.setColor("Color", ColorRGBA.Green);
        } else {
            material.setColor("Color", ColorRGBA.DarkGray);
        }
        geo.setMaterial(material);
        manipulators.put(state.getManipulatorModel().getName(), geo);
    }

    public void createAxon(State state, float x, float y, float z) {
        HashSet<AxonModel> axonModels = state.getAxonModels();
        float xEnd = x;
        float xPlus = widthAxon;
        if (axonModels.size() > 1) {
            xEnd = x - (widthAxon / 2);
            xPlus = widthAxon / ((float) (axonModels.size() - 1));
        }


        for (AxonModel axonModel : axonModels) {
            Geometry geo = (Geometry) spatials.get(axonModel.getName());
            if (geo == null) {
                Vector3f start = new Vector3f(x, y, z);
                Vector3f end = new Vector3f(xEnd, y + heightAxon, z);
                Line line = new Line(start, end);
                geo = new Geometry(axonModel.getName(), line);
                spatials.put(geo.getName(), geo);
                putToNode(geo, state.getNeuronModel().getName());

                Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                material.setColor("Color", ColorRGBA.Gray);
                geo.setMaterial(material);
            }

//            setActiveMaterial(geo, axonModel.getActive(), power);

            createSenders(state, xEnd, (y + heightAxon), z, axonModel.getId(), axonModel.getActive());

            xEnd += xPlus;
        }
    }

    private void createSenders(State state, float x, float y, float z, long axonId, boolean active) {
        HashSet<SenderModel> senderModels = state.getSenderModels();

        for (SenderModel senderModel : senderModels) {
            Vector3f end;
            if (Layer.LayerType.MANIPULATOR.ordinal() == state.getLayerModel().getType()) {
                Geometry manipulator = (Geometry) manipulators.get(state.getManipulatorModel().getName());
                if (manipulator == null) continue;
                end = manipulator.getWorldTranslation();

            } else {
                if (senderModel.getAxon_id() != axonId
                        || senderModel.getReceiver_id() == null
                        || senderModel.getRlayer_id() == null
                        || senderModel.getRneuron_id() == null
                        || senderModel.getRdendrite_id() == null) continue;

                ReceiverModel rm = new ReceiverModel();
                rm.setId(senderModel.getReceiver_id());
                rm.setLayer_id(senderModel.getRlayer_id());
                rm.setNeuron_id(senderModel.getRneuron_id());
                rm.setDendrite_id(senderModel.getRdendrite_id());
                end = receiversCoordinate.get(rm.getKey());
            }
            if (end == null) continue;

            Geometry geo = (Geometry) spatials.get(senderModel.getName());
            if (geo == null) {
                Vector3f start = new Vector3f(x, y, z);
                Line line = new Line(start, end);
                geo = new Geometry(senderModel.getName(), line);
                spatials.put(geo.getName(), geo);
                putToNode(geo, state.getNeuronModel().getName());
            }

            int power = 0;
            for (TransmitterModel transmitterModel : state.getTransmitterModels()) {
                if (!Objects.equals(transmitterModel.getSender_id(), senderModel.getId())) continue;
                for (Synapse.Type t : Synapse.Type.values()) {
                    if (t.ordinal() == transmitterModel.getType()) {
                        switch (t) {
                            case ACTIVATOR:
                                power++;
                                break;
                            case INHIBITOR:
                                power--;
                                break;
                        }
                        break;
                    }
                }
            }

            setActiveMaterial(geo, active, power);
        }
    }

    private void setActiveMaterial(Geometry geo, boolean active, int power) {
        if (active) {
            if (power > 0) {
                Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                material.setColor("Color", new ColorRGBA(1, 0.5f, 0.5f, 0));
                geo.setMaterial(material);
            } else if (power < 0) {
                Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                material.setColor("Color", new ColorRGBA(0.5f, 0.5f, 1, 0));
                geo.setMaterial(material);
            } else {
                Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                material.setColor("Color", new ColorRGBA(0.5f, 1, 0.5f, 0));
                geo.setMaterial(material);
            }
        } else {
            Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            material.setColor("Color", ColorRGBA.Brown);
            geo.setMaterial(material);
        }
    }

    public void createDendrite(State state, float x, float y, float z) {
        HashSet<DendriteModel> dendriteModels = state.getDendriteModels();
        float xEnd = x;
        float xPlus = widthDendrite;
        if (dendriteModels.size() > 1) {
            xEnd = x - (widthDendrite / 2);
            xPlus = widthDendrite / ((float) (dendriteModels.size() - 1));
        }


        for (DendriteModel dendriteModel : dendriteModels) {
            Geometry geo = (Geometry) spatials.get(dendriteModel.getName());
            if (geo == null) {
                geo = createLine(x, y, xEnd, y - heightDendrite, dendriteModel.getName(), z);
                spatials.put(geo.getName(), geo);
                putToNode(geo, state.getNeuronModel().getName());
            }

            createReceivers(state, xEnd, (y - heightDendrite), xPlus * 0.8f, dendriteModel.getId(), z);

            xEnd += xPlus;
        }
    }

    public void createReceivers(State state, float x, float y, float width, long dendriteId, float z) {
        HashSet<ReceiverModel> receiverModels = state.getReceiverModels();
        if (receiverModels == null || receiverModels.isEmpty()) return;
        int receiversCount = 0;
        for (ReceiverModel model : receiverModels) {
            if (model.getDendrite_id() != dendriteId) continue;
            receiversCount++;
        }

        float xEnd = x;
        float xPlus = width;
        if (receiverModels.size() > 1) {
            xEnd = x - (width / 2);
            xPlus = width / ((float) (receiversCount - 1));
        }


        for (ReceiverModel receiverModel : receiverModels) {
            if (receiverModel.getDendrite_id() != dendriteId) continue;
            Geometry geo = (Geometry) spatials.get(receiverModel.getName());
            if (geo == null) {
                geo = createLine(x, y, xEnd, y - heightReceiver, receiverModel.getName(), z);
                receiversCoordinate.put(receiverModel.getKey(), new Vector3f(xEnd, y - heightReceiver, z));
                spatials.put(geo.getName(), geo);
                putToNode(geo, state.getNeuronModel().getName());
            }

            createReceptors(state, xEnd, (y - heightReceiver), receiverModel.getId(), receiverModel.getDendrite_id(), z);

            xEnd += xPlus;
        }
    }

    private Geometry createLine(float xStart, float yStart, float xEnd, float yEnd, String name, float z) {
        Vector3f start = new Vector3f(xStart, yStart, z);
        Vector3f end = new Vector3f(xEnd, yEnd, z);
        Line line = new Line(start, end);
        Geometry geo = new Geometry(name, line);

        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        material.setColor("Color", ColorRGBA.Gray);
        geo.setMaterial(material);
        return geo;
    }

    public void createReceptors(State state, float x, float y, long receiverId, long dendriteId, float z) {
        HashSet<ReceptorModel> receptorModels = state.getReceptorModels();
        if (receptorModels == null) return;
        for (ReceptorModel receptorModel : receptorModels) {
            if (receptorModel.getReceiver_id() != receiverId || receptorModel.getDendrite_id() != dendriteId) continue;
            Geometry geo = (Geometry) spatials.get(receptorModel.getName());
            if (geo == null) {
                Sphere sphereMesh = new Sphere(5, 5, 0.1f);
                geo = new Geometry(receptorModel.getName(), sphereMesh);
                Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                material.setColor("Color", ColorRGBA.Gray);
                geo.setMaterial(material);
                geo.setLocalTranslation(x, y, z);
                spatials.put(geo.getName(), geo);
                putToNode(geo, state.getNeuronModel().getName());
            }
        }
    }

    public void createNeuronBody(State state, float x, float y, float z) {
        NeuronModel neuronModel = state.getNeuronModel();

        Geometry geo = (Geometry) spatials.get(neuronModel.getName());
        if (geo == null) {
            Sphere sphereMesh = new Sphere(8, 8, 1f);
            geo = new Geometry(neuronModel.getName(), sphereMesh);
            geo.setLocalTranslation(x, y, z);
            spatials.put(geo.getName(), geo);
            putToNode(geo, state.getNeuronModel().getName());

        }

        //-----TEXT-----
        BitmapText text = (BitmapText) spatials.get("text" + neuronModel.getName());
        if (text == null) {
            BitmapFont font = assetManager.loadFont("Interface/Fonts/Default.fnt");
            text = new BitmapText(font);
            text.setSize(0.5f);

            text.setColor(ColorRGBA.Green);
            text.setLocalTranslation(x + 1, y + 1, z + 1);
            spatials.put("text" + neuronModel.getName(), text);
            putToNode(text, state.getNeuronModel().getName());
        }
        text.setText(geo.getName() + " " +
                neuronModel.getAmountep() + "/"
                + (neuronModel.getRefres() + state.getNeuronModel().getResist()));

        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        ColorRGBA rgba = createColorFromEP(state);
        material.setColor("Color", rgba);
        geo.setMaterial(material);

    }

    public ColorRGBA createColorFromEP(State state) {
        Integer refres = state.getNeuronModel().getRefres();
        if (refres == null) refres = 0;
        float resist = refres + state.getNeuronModel().getResist();
        float r = (state.getNeuronModel().getAmountep() / resist);
        if (state.getNeuronModel().getAmountep() > 0) {
            if (r > 1) r = 1;
            return new ColorRGBA(1, (1 - r), (1 - r), 0);
        } else {
            r = -r;
            if (r > 1) r = 1;
            return new ColorRGBA((1 - r), (1 - r), 1, 0);
        }
    }

    public HashMap<String, State> getStates() {
        return states;
    }

    public HashMap<String, Spatial> getSpatials() {
        return spatials;
    }

    public boolean isReaded() {
        return readed;
    }

    public void setReaded(boolean readed) {
        this.readed = readed;
    }

    public boolean isRun() {
        return run;
    }

    public void setRun(boolean run) {
        this.run = run;
    }

    public HashMap<String, Node> getNodes() {
        return nodes;
    }
}
