package pro.belbix.bbvisual;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;

/**
 * Created by v.belykh on 25.07.2018
 */
public class NeuronUpdater extends AbstractAppState {
    private static final Logger log = LogManager.getRootLogger();
    private NeuronBuilder neuronBuilder = null;
    private AssetManager assetManager;
    private HashMap<String, Node> attachedNode = new HashMap<>();
    private Node rootNode = new Node("NeuronUpdaterRootNode");

    public NeuronUpdater(AssetManager assetManager) {
        this.assetManager = assetManager;
        init();
    }

    private void init(){
        neuronBuilder = new NeuronBuilder(assetManager);
        Thread t = new Thread(neuronBuilder);
        t.setName("NeuronBuilder");
        t.start();
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (neuronBuilder.readStates()) {
            LocalDateTime start = LocalDateTime.now();
//            log.debug("update...");
            for (String name : attachedNode.keySet()) {
                if (!neuronBuilder.getNodes().containsKey(name)) {
                    log.trace("detach " + name);
                    rootNode.detachChildNamed(name);
                    attachedNode.remove(name);
                }
            }
            for (String name : neuronBuilder.getNodes().keySet()) {
                Node node = neuronBuilder.getNodes().get(name);
                if (rootNode.getChild(node.getName()) != null) rootNode.detachChildNamed(node.getName());
                rootNode.attachChild(node);
                attachedNode.put(name, node);
            }
            log.debug("update neurons by time : " + Duration.between(start, LocalDateTime.now()));
        }

        rootNode.updateLogicalState(tpf);
        rootNode.updateGeometricState();
    }

    public Node getRootNode() {
        return rootNode;
    }

    @Override
    public void cleanup() {
        super.cleanup();
        close();
    }

    private void close() {
        neuronBuilder.setRun(false);
    }}
