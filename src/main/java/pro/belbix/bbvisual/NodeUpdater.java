package pro.belbix.bbvisual;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Sphere;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.models.*;
import pro.belbix.bbrain.neuron.Layer;

import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

import static pro.belbix.bbvisual.Config.*;

/**
 * Created by v.belykh on 13.07.2018
 */
public class NodeUpdater {
    private static final Logger log = LogManager.getRootLogger();
    private BitmapFont font;
    private Material defaultMaterial;
    private Map<String, Spatial> global;
    private Node node;
    private State state;
    private Vector3f centerPoint;

    public NodeUpdater(Node node, Map<String, Spatial> global, AssetManager assetManager) {
        this.global = global;
        this.node = node;

        this.font = assetManager.loadFont("Interface/Fonts/Default.fnt");
        this.defaultMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    }

    public void updateNodeFromState(State state) {
//        if (state.getTransmitterModels() == null
//                || state.getNeuronModel() == null
//                || state.getSenderModels() == null
//                || state.getAxonModels() == null
//                || state.getDendriteModels() == null
//                || state.getReceiverModels() == null
//                || state.getReceptorModels() == null
//                || state.getLayerModel() == null) {
//            log.warn("Not valid state! " + state);
//            return;
//        }
        this.state = state;
        centerPoint = createPoint(state);
        if (centerPoint == null) return;


        //--------------------MANIPULATOR BODY------------------------------
        if (Layer.LayerType.MANIPULATOR.ordinal() == state.getLayerModel().getType()
                && state.getManipulatorModel() != null
                ) {

            updateManipulator();
        }

        //--------------------NEURON BODY------------------------------
        updateNeuronBody();

        //--------------------DENDRITE------------------------------
        updateDendrites();

        //--------------------AXON------------------------------
        updateAxons();
    }

    private Vector3f createPoint(State state) {
        long lCount = state.getNeuronModel().getLayer_id();
        long nId = state.getNeuronModel().getId();
        float median = 0;//(maxLayerCount - lCount) / 2;
        float xStart = 0;// median * Config.widthNeuron;
        if (Config.zLines) xStart = FastMath.sqrt(xStart);

        LayerModel layerModel = state.getLayerModel();
        if (layerModel == null) {
            log.warn("null layer " + state);
            return null;
        }
        float line = FastMath.floor(FastMath.sqrt(layerModel.getNcount()));

        float nnZ = FastMath.floor(nId / line);
        long nnId = nId;
        if (Config.zLines) nnId = nId - (long) (nnZ * line);

        if (Layer.LayerType.MANIPULATOR.ordinal() == state.getLayerModel().getType()) xStart = 0;


        float x = xStart + (nnId * Config.widthNeuron);
        float y = (state.getNeuronModel().getLayer_id() * Config.heightLayer);
        float z = 0;
        if (Config.zLines) z = nnZ * Config.widthNeuron;
        return new Vector3f(x, y, z);
    }

    private void updateManipulator() {
        ManipulatorModel manipulatorModel = state.getManipulatorModel();
        if (manipulatorModel == null) return;

        Vector3f mPoint = centerPoint.add(new Vector3f(0, 4f, 0));

        String textName = "text" + manipulatorModel.getName();
        String text = manipulatorModel.getName() + " "
                + manipulatorModel.getPower() + "/"
                + manipulatorModel.getResist() + " "
                + manipulatorModel.getDecision();
        Vector3f textPoint = mPoint.add(new Vector3f(2f, 2f, 1f));

        Geometry geo = (Geometry) node.getChild(manipulatorModel.getName());
        BitmapText bitmapText;
        Boolean oldDecision;
        if (geo == null) {
            Sphere sphereMesh = new Sphere(8, 8, 2f);
            geo = new Geometry(manipulatorModel.getName(), sphereMesh);
            geo.setLocalTranslation(mPoint);
            geo.setUserData("decision", manipulatorModel.getDecision());
            oldDecision = manipulatorModel.getDecision();

            bitmapText = new BitmapText(font);
            bitmapText.setSize(0.5f);
            bitmapText.setColor(ColorRGBA.Gray);
            bitmapText.setLocalTranslation(textPoint);
            bitmapText.setName(textName);

            node.attachChild(bitmapText);
            node.attachChild(geo);
            global.put(manipulatorModel.getName(), geo);
        } else {
            oldDecision = geo.getUserData("decision");
            bitmapText = (BitmapText) node.getChild(textName);
        }


        if (!bitmapText.getText().equals(text)) {
            bitmapText.setText(text);
        }

        if (!manipulatorModel.getDecision().equals(oldDecision)) {
            Material materialClone = defaultMaterial.clone();
            if (manipulatorModel.getDecision()) {
                materialClone.setColor("Color", ColorRGBA.Green);
            } else {
                materialClone.setColor("Color", ColorRGBA.DarkGray);
            }
            geo.setMaterial(materialClone);
        }

    }

    private void updateAxons() {
        HashSet<AxonModel> axonModels = state.getAxonModels();
        float xEnd = centerPoint.getX();
        float xPlus = widthAxon;
        if (axonModels.size() > 1) {
            xEnd = centerPoint.getX() - (widthAxon / 2);
            xPlus = widthAxon / ((float) (axonModels.size() - 1));
        }


        for (AxonModel axonModel : axonModels) {
            Geometry geo = (Geometry) node.getChild(axonModel.getName());
            Vector3f end = new Vector3f(xEnd, centerPoint.getY() + heightAxon, centerPoint.getZ());
            Boolean oldActive;
            if (geo == null) {
                Line line = new Line(centerPoint, end);
                geo = new Geometry(axonModel.getName(), line);
                Material materialClone = defaultMaterial.clone();
                materialClone.setColor("Color", ColorRGBA.Gray);
                geo.setMaterial(materialClone);
                geo.setUserData("active", axonModel.getActive());
                oldActive = axonModel.getActive();

                node.attachChild(geo);
                updateSenders(end, axonModel.getId(), axonModel.getActive());
            } else {
                oldActive = geo.getUserData("active");
            }

            if (!axonModel.getActive().equals(oldActive)) {
                geo.setUserData("active", axonModel.getActive());
                updateSenders(end, axonModel.getId(), axonModel.getActive());
            }

            xEnd += xPlus;
        }
    }

    private Vector3f getSenderDestination(SenderModel senderModel, long axonId) {
        Vector3f end = null;
        if (Layer.LayerType.MANIPULATOR.ordinal() == state.getLayerModel().getType()) {
            if (state.getManipulatorModel() == null) return null;
            Spatial manipulator = global.get(state.getManipulatorModel().getName());
            if (manipulator != null) {
                end = manipulator.getWorldTranslation();
            }
        } else {
            if (senderModel.getAxon_id() != axonId
                    || senderModel.getReceiver_id() == null
                    || senderModel.getRlayer_id() == null
                    || senderModel.getRneuron_id() == null
                    || senderModel.getRdendrite_id() == null) return null;

            String rName = ReceiverModel.createName(senderModel.getRlayer_id(),
                    senderModel.getRneuron_id(),
                    senderModel.getRdendrite_id(),
                    senderModel.getReceiver_id());
            Geometry receiverSpatial = (Geometry) global.get(rName);
            if (receiverSpatial != null) {
                Line line = (Line)receiverSpatial.getMesh();
                end = line.getEnd();
            }
        }
        return end;
    }

    private void updateSenders(Vector3f point, long axonId, boolean active) {
        HashSet<SenderModel> senderModels = state.getSenderModels();

        for (SenderModel senderModel : senderModels) {
            Vector3f end = getSenderDestination(senderModel, axonId);
            if (end == null) continue;

            Integer power;
            Geometry geo = (Geometry) node.getChild(senderModel.getName());
            if (geo == null) {
                Vector3f start = new Vector3f(point);
                Line line = new Line(start, end);
                geo = new Geometry(senderModel.getName(), line);
                power = getPower(senderModel, state);
                geo.setUserData("power", power);
                node.attachChild(geo);
            } else {
                power = geo.getUserData("power");
            }

            setActiveMaterial(geo, active, power);
        }
    }

    private int getPower(SenderModel senderModel, State state) {
        int power = 0;
        for (TransmitterModel transmitterModel : state.getTransmitterModels()) {
            if (!Objects.equals(transmitterModel.getSender_id(), senderModel.getId())) continue;
            for (Synapse.Type t : Synapse.Type.values()) {
                if (t.ordinal() == transmitterModel.getType()) {
                    switch (t) {
                        case ACTIVATOR:
                            power++;
                            break;
                        case INHIBITOR:
                            power--;
                            break;
                    }
                    break;
                }
            }
        }
        return power;
    }

    private void setActiveMaterial(Geometry geo, boolean active, int power) {
        if (active) {
            if (power > 0) {
                Material material = defaultMaterial.clone();
                material.setColor("Color", new ColorRGBA(1, 0.5f, 0.5f, 0));
                geo.setMaterial(material);
            } else if (power < 0) {
                Material material = defaultMaterial.clone();
                material.setColor("Color", new ColorRGBA(0.5f, 0.5f, 1, 0));
                geo.setMaterial(material);
            } else {
                Material material = defaultMaterial.clone();
                material.setColor("Color", new ColorRGBA(0.5f, 1, 0.5f, 0));
                geo.setMaterial(material);
            }
        } else {
            Material material = defaultMaterial.clone();
            material.setColor("Color", ColorRGBA.Brown);
            geo.setMaterial(material);
        }
    }

    private void updateDendrites() {
        HashSet<DendriteModel> dendriteModels = state.getDendriteModels();
        float xEnd = centerPoint.getX();
        float xPlus = widthDendrite;
        if (dendriteModels.size() > 1) {
            xEnd = centerPoint.getX() - (widthDendrite / 2);
            xPlus = widthDendrite / ((float) (dendriteModels.size() - 1));
        }

        for (DendriteModel dendriteModel : dendriteModels) {
            Vector3f end = new Vector3f(xEnd, centerPoint.getY() - heightDendrite, centerPoint.getZ());
            Geometry geo = (Geometry) node.getChild(dendriteModel.getName());
            if (geo == null) {
                geo = createLine(centerPoint, end, dendriteModel.getName());
                node.attachChild(geo);
            }

            updateReceivers(end, xPlus * 0.8f, dendriteModel.getId());

            xEnd += xPlus;
        }
    }

    private void updateReceivers(Vector3f point, float width, long dendriteId) {
        HashSet<ReceiverModel> receiverModels = state.getReceiverModels();
        if (receiverModels == null || receiverModels.isEmpty()) return;
        int receiversCount = 0;
        for (ReceiverModel model : receiverModels) {
            if (model.getDendrite_id() != dendriteId) continue;
            receiversCount++;
        }

        float xEnd = point.getX();
        float xPlus = width;
        if (receiverModels.size() > 1) {
            xEnd = point.getX() - (width / 2);
            xPlus = width / ((float) (receiversCount - 1));
        }

        for (ReceiverModel receiverModel : receiverModels) {
            if (receiverModel.getDendrite_id() != dendriteId) continue;
            Geometry geo = (Geometry) node.getChild(receiverModel.getName());
            Vector3f end = new Vector3f(xEnd, point.getY() - heightReceiver, point.getZ());
            if (geo == null) {
                geo = createLine(point, end, receiverModel.getName());
                updateReceptors(end, receiverModel.getId(), dendriteId);
                node.attachChild(geo);
                global.put(receiverModel.getName(), geo);
            }
            xEnd += xPlus;
        }
    }

    private Geometry createLine(Vector3f start, Vector3f end, String name) {
        Line line = new Line(start, end);
        Geometry geo = new Geometry(name, line);
        Material materialClone = defaultMaterial.clone();
        materialClone.setColor("Color", ColorRGBA.Gray);
        geo.setMaterial(materialClone);
        return geo;
    }

    private void updateReceptors(Vector3f point, long receiverId, long dendriteId) {
        HashSet<ReceptorModel> receptorModels = state.getReceptorModels();
        if (receptorModels == null) return;
        for (ReceptorModel receptorModel : receptorModels) {
            if (receptorModel.getReceiver_id() != receiverId || receptorModel.getDendrite_id() != dendriteId) continue;
            Geometry geo = (Geometry) node.getChild(receptorModel.getName());
            if (geo == null) {
                Sphere sphereMesh = new Sphere(5, 5, 0.1f);
                geo = new Geometry(receptorModel.getName(), sphereMesh);
                Material materialClone = defaultMaterial.clone();
                materialClone.setColor("Color", ColorRGBA.Gray);
                geo.setMaterial(materialClone);
                geo.setLocalTranslation(point);
                node.attachChild(geo);
            }
        }
    }

    private void updateNeuronBody() {
        NeuronModel neuronModel = state.getNeuronModel();


        String textName = "text" + neuronModel.getName();
        String text = neuronModel.getName() + " " +
                neuronModel.getAmountep() + "/"
                + (neuronModel.getRefres() + state.getNeuronModel().getResist());


        Geometry geo = (Geometry) node.getChild(neuronModel.getName());
        BitmapText bitmapText;
        Integer refres;
        Long resist;
        Long amountep;
        if (geo == null) {
            Sphere sphereMesh = new Sphere(8, 8, 1f);
            geo = new Geometry(neuronModel.getName(), sphereMesh);
            geo.setLocalTranslation(centerPoint);

            refres = neuronModel.getRefres();
            resist = neuronModel.getResist();
            amountep = neuronModel.getAmountep();
            geo.setUserData("refres", refres);
            geo.setUserData("resist", resist);
            geo.setUserData("amountep", amountep);


            setColor(geo, createColorFromEP(neuronModel));

            Vector3f textPoint = centerPoint.add(new Vector3f(1f, 1f, 1f));
            bitmapText = new BitmapText(font);
            bitmapText.setSize(0.5f);
            bitmapText.setColor(ColorRGBA.Green);
            bitmapText.setLocalTranslation(textPoint);
            bitmapText.setName(textName);

            node.attachChild(bitmapText);
            node.attachChild(geo);
        } else {
            refres = geo.getUserData("refres");
            resist = geo.getUserData("resist");
            amountep = geo.getUserData("amountep");
            bitmapText = (BitmapText) node.getChild(textName);
        }

        if (!bitmapText.getText().equals(text)) {
            bitmapText.setText(text);
        }

        if (!neuronModel.getRefres().equals(refres)
                || !neuronModel.getResist().equals(resist)
                || !neuronModel.getAmountep().equals(amountep)
                ) {
            refres = neuronModel.getRefres();
            resist = neuronModel.getResist();
            amountep = neuronModel.getAmountep();
            geo.setUserData("refres", refres);
            geo.setUserData("resist", resist);
            geo.setUserData("amountep", amountep);
            setColor(geo, createColorFromEP(neuronModel));
        }
    }

    private void setColor(Geometry geo, ColorRGBA rgba) {
        Material materialClone = defaultMaterial.clone();
        materialClone.setColor("Color", rgba);
        geo.setMaterial(materialClone);
    }

    private ColorRGBA createColorFromEP(NeuronModel neuronModel) {
        Integer refres = neuronModel.getRefres();
        if (refres == null) refres = 0;
        float resist = refres + neuronModel.getResist();
        float r = (neuronModel.getAmountep() / resist);
        if (neuronModel.getAmountep() > 0) {
            if (r > 1) r = 1;
            return new ColorRGBA(1, (1 - r), (1 - r), 0);
        } else {
            r = -r;
            if (r > 1) r = 1;
            return new ColorRGBA((1 - r), (1 - r), 1, 0);
        }
    }
}
