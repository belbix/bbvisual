package pro.belbix.bbvisual;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.models.Key;
import pro.belbix.bbrain.models.State;
import pro.belbix.bbvisual.kafka.ConsumerCreator;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * Created by v.belykh on 9/20/2018
 */
public class Receiver implements Callable<Integer> {
    private static final Logger log = LogManager.getRootLogger();
    private Consumer<Long, State> consumer = ConsumerCreator.createConsumer();
    private Node rootNode = null;
    private ConcurrentHashMap<Key, Node> nodes = null;
    private ConcurrentHashMap<String, Spatial> globalSpatial = null;
    private AssetManager assetManager;
    private int count = 0;
    private int countSkip = 0;
    private int number;

    public Receiver(Node rootNode,
                    ConcurrentHashMap<Key, Node> nodes,
                    ConcurrentHashMap<String, Spatial> globalSpatial,
                    AssetManager assetManager,
                    int number) {
        this.rootNode = rootNode;
        this.nodes = nodes;
        this.globalSpatial = globalSpatial;
        this.assetManager = assetManager;
        this.number = number;
        consumer.subscribe(Pattern.compile(pro.belbix.bbrain.utils.Config.partitionPattern));
    }

    @Override
    public Integer call() throws Exception {
        LocalDateTime start = LocalDateTime.now();

        ConsumerRecords<Long, State> consumerRecords = consumer.poll(Duration.ofMillis(1));

        count = 0;
        countSkip = 0;
        consumerRecords.forEach(record -> {
            LocalDateTime rTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(record.timestamp()), ZoneOffset.systemDefault());
            if (Duration.between(rTime, LocalDateTime.now()).getSeconds() > 5) {
                countSkip++;
                return;
            }
            State state = record.value();
            count++;
            Node node = nodes.get(state.getKey());
            if (node == null) {
                node = new Node();
                rootNode.attachChild(node);
                nodes.put(state.getKey(), node);
            }
            NodeUpdater nodeUpdater = new NodeUpdater(node, globalSpatial, assetManager);
            nodeUpdater.updateNodeFromState(state);
        });
        consumer.commitAsync();
        if (count + countSkip > 0) {
            log.debug("update " + count + " (skip " + countSkip + ") neurons of(" + nodes.size() + ") by time : "
                    + Duration.between(start, LocalDateTime.now()));
        }
        return count + countSkip;
    }
}
