package pro.belbix.bbvisual.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.models.State;

import java.util.Map;

/**
 * Created by v.belykh on 9/18/2018
 */
public class StateDeserializer implements Deserializer<State> {
    private static final Logger log = LogManager.getRootLogger();
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public State deserialize(String s, byte[] bytes) {
        State state = null;
        try {
            state = mapper.readValue(bytes, State.class);
        } catch (Exception e) {
            log.error("Error deserialize " + new String(bytes), e);
        }
        return state;
    }

    @Override
    public void close() {

    }
}
